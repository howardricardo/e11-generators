from main import exercise_1, exercise_1_yield


def test_exercise_1():
    max_range = 6
    expected = [0, 1, 2, 3, 4, 5]

    result = exercise_1(max_range)
    assert result == expected


def test_exercise_1_yield():
    max_range = 6
    expected = [0, 1, 2, 3, 4, 5]

    result = exercise_1(max_range)
    assert result == expected
